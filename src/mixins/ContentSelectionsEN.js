export default {
  data: function () {
    return {
      contentSelectionsEN: [
        {
          type: 'video',
          title: 'Nothing to hide',
          thumbnailName: 'nothing-to-hide.jpg',
          url: 'https://media.zat.im/videos/watch/35badfed-5322-48ac-b5c1-71b1ad88262e',
          tags: [ '#privacy', '#documentary' ],
          description: 'Nothing to hide (2017) is a Franco-German feature-length documentary by Marc Meillassoux and Mihaela Gladovic, about how mass surveillance affects individuals and society. Taking a critical look at the laws allowing State surveillance that were implemented by several countries in the past few years, we are reminded of how important the debate about usage of personal data is and how it questions the very basis of democracy.'
        }
      ]
    }
  }
}
